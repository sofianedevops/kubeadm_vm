#! /bin/bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io
apt-get install -y kubelet=1.24.9-00 kubeadm=1.24.9-00 kubectl=1.24.9-00
apt-mark hold kubelet kubeadm kubectl
systemctl start docker
systemctl enable docker
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
echo '{
  "exec-opts": ["native.cgroupdriver=systemd"]
}'>/etc/docker/daemon.json
echo "net.ipv4.conf.all.forwarding=1"> /etc/sysctl.d/enable_ipv4_forwading.conf
echo "net.bridge.bridge-nf-call-iptables = 1"> /etc/sysctl.d/k8s.conf
echo "apiVersion: 'kubeadm.k8s.io/v1beta2'
kubernetesVersion: 1.24.9
kind: ClusterConfiguration
networking:
  podSubnet: $(ip route | grep eth1 | grep -v default | awk '{print $1}')
controlPlaneEndpoint: k8s-master:6443" > /tmp/kubeadm-init.yaml
containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sed 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
systemctl restart containerd
kubeadm init --config /tmp/kubeadm-init.yaml --upload-certs | tee /var/log/kubeadm-init.log
